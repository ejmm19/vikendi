const modal = '#add-edit-docentes';
new Vue({
    el: '#app-docentes',
    data: {
        message: 'hola a todos',
        data_modal: {
            formData: {
                name: '',
                document: '',
                edad: '',
                email: '',
                grado: ''
            },
            docenteToEdit: {
                id: '',
                name: '',
                document: '',
                edad: '',
                email: '',
                grado: ''
            },
            emailValid: '',
            emailValidError: 'Correo electrónico no válido',
            documentValid: '',
            documentValidError: 'Documento no válido, solo se permiten números',
            nameValid: '',
            edadValid: '',
            gradoValid: '',
            formValid: false,
            type: '',
            error: false,
            message: ''
        }
    },
    watch: {
        data_modal: {
            handler(val){
                let email = val.formData.email;
                if (email.length > 3){
                    this.data_modal.emailValid = this.validateEmail(email);
                }else if(email.length === 0){
                    this.data_modal.emailValid = '';
                }

                let document = val.formData.document;
                if(document.length === 0){
                    this.data_modal.documentValid = '';
                }else{
                    this.data_modal.documentValid = this.validateNumbers(document);
                }
                let name = val.formData.name;
                this.data_modal.nameValid = name.length !== 0;
                let grado = val.formData.grado;
                this.data_modal.gradoValid = grado.length !== 0;

                let edad = val.formData.edad;
                this.data_modal.edadValid = edad.length !== 0;

                this.data_modal.formValid = !!(this.data_modal.documentValid === true && this.data_modal.emailValid && this.data_modal.nameValid === true
                     && this.data_modal.edadValid === true && this.data_modal.gradoValid === true);

            },
            deep: true
        }
    },
    methods: {
        nuevo: function (){
            this.data_modal.type = 'new';
            this.showModal();
        },
        edit: function (id, email, name, document, grado, edad){
            this.data_modal.type = 'edit';
            this.data_modal.docenteToEdit.id = id;
            this.data_modal.docenteToEdit.email = email;
            this.data_modal.docenteToEdit.name = name;
            this.data_modal.docenteToEdit.document = document;
            this.data_modal.docenteToEdit.edad = edad;
            this.data_modal.formData.email = email;
            this.data_modal.formData.name = name;
            this.data_modal.formData.document = document;
            this.data_modal.formData.edad = edad;
            this.data_modal.formData.grado = grado;
            this.showModal();
        },
        validateEmail: function (email) {
            const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        },
        validateNumbers: function (val) {
            let reg = new RegExp('^[0-9]+$');
            return reg.test(val);
        },
        showModal: function () {
            $(modal).modal('show');
        },
        hideModal: function (){
            $(modal).modal('hide');
        },
        saveData: function (){
            let data;
            if (this.data_modal.type === 'new'){
                data = {
                    name : this.data_modal.formData.name,
                    document: this.data_modal.formData.document,
                    email: this.data_modal.formData.email,
                    grado_id: this.data_modal.formData.grado,
                    edad: this.data_modal.formData.edad
                }
            }else{
                data = {
                    id: this.data_modal.docenteToEdit.id,
                    name : this.data_modal.formData.name,
                    grado_id: this.data_modal.formData.grado,
                    edad: this.data_modal.formData.edad
                }
            }
            this.sendData(data, this.data_modal.type);
        },
        sendData: function (data, type) {
            console.log(data);
            let _URL = (type === 'new') ? '/docentes/create/' : '/docentes/update/'+data.id;
            axios.post(_URL, data)
                .then( (response) => {
                    console.log(response.data);
                    if (response.data.statusText === 'OK'){
                        location.reload();
                    }else if(response.data.statusText === 'ERROR'){
                        this.data_modal.error = true;
                        this.data_modal.message = response.data.message;
                        this.clearForm();
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        remove: function (id) {
            Swal.fire({
                title: 'Está seguro de eliminar?',
                text: "Si aceptas se reliminará el registro y no se podrá recuperar",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, Eliminarlo!'
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.post('/docentes/delete/'+id)
                        .then((response) => {
                            if (response.data.statusText === 'OK'){
                                Swal.fire(
                                    response.data.message,
                                    '',
                                    'success'
                                ).then(() =>{
                                    location.reload();
                                })
                            }else if(response.data.statusText === 'ERROR'){
                                Swal.fire(
                                    response.data.message,
                                    '',
                                    'error'
                                )
                                this.data_modal.error = true;
                                this.data_modal.message = response.data.message;
                                // this.clearForm();
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                }
            })
        },
    }
});
$(document).ready( function () {
    $('#docentes_table').on('init.dt',function() {
        $("#docentes_table").removeClass('table-loader').show();
    });
    setTimeout(function(){
        $('#docentes_table').DataTable();
    }, 3000);
} );
