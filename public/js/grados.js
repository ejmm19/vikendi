const modal = '#add-edit-grado';
new Vue({
    el: '#app-grados',
    data: {
        message: 'este es el mensaje señor',
        data_modal: {
            type: null,
            selectGrado: '',
            selectGrupo: '',
            formData: {
                id: '',
                name: ''
            },
            gradoToEdit: {
              id: '',
              name: ''
            },
            error: false,
            message: ''
        },
    },
    methods: {
        saveData: function (){
            let data;
            if (this.data_modal.type === 'new'){
                data = {
                    name : this.data_modal.formData.name
                }
            }else{
                data = {
                    id: this.data_modal.gradoToEdit.id,
                    name : this.data_modal.formData.name
                }
            }
            this.sendData(data, this.data_modal.type);
        },
        validate: function () {
            if (this.data_modal.selectGrado !== '' && this.data_modal.selectGrupo !== ''){
                this.data_modal.formData.name = this.data_modal.selectGrado + '-' + this.data_modal.selectGrupo;
            }
        },
        edit: function (id, name){
            this.data_modal.type = 'edit';
            this.data_modal.gradoToEdit.name = name;
            this.data_modal.gradoToEdit.id = id;
            this.showModal();
        },
        remove: function (id) {
            Swal.fire({
                title: 'Esta seguro de eliminar el grado?',
                text: "Esta acción no se podrá revertir, tenga en cuenta que esto eliminará a los docentes y estudiantes que esten en este curso",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminarlo!'
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.post('/grados/delete/'+id)
                        .then((response) => {
                            console.log(response);
                            if (response.data.statusText === 'OK'){
                                Swal.fire(
                                    'Eliminado!',
                                    response.data.message,
                                    'success'
                                ).then(() =>{
                                    location.reload();
                                })
                            }else if(response.data.statusText === 'ERROR'){
                                this.data_modal.error = true;
                                this.data_modal.message = response.data.message;
                                this.clearForm();
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                }
            })
        },
        nuevo: function (event){
            this.data_modal.type = 'new';
            this.showModal();
        },
        showModal: function (){
            $(modal).modal('show');
        },
        hideModal: function (){
            $(modal).modal('hide');
        },
        clearForm: function () {
            this.data_modal.formData.name = '';
        },
        sendData: function (data, type) {
            console.log(data);
            let _URL = (type === 'new') ? '/grados/create/' : '/grados/update/'+data.id;
            axios.post(_URL, data)
                .then( (response) => {
                    console.log(response.data);
                    if (response.data.statusText === 'OK'){
                        location.reload();
                    }else if(response.data.statusText === 'ERROR'){
                        this.data_modal.error = true;
                        this.data_modal.message = response.data.message;
                        this.clearForm();
                    }
                })
            .catch(function (error) {
                console.log(error);
            });
        }
    }
});
$(document).ready( function () {
    $('#grados_table').on('init.dt',function() {
        $("#grados_table").removeClass('table-loader').show();
    });
    setTimeout(function(){
        $('#grados_table').DataTable();
    }, 3000);
} );

$(document).ready( function () {
    $('#grados_table').DataTable();
} );
