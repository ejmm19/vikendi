const modal = '#add-edit-estudiantes';
new Vue({
    el: '#app-estudiantes',
    data: {
        message: 'hola a todos',
        data_modal: {
            formData: {
                name: '',
                document: '',
                edad: '',
                grado: ''
            },
            estudianteToEdit: {
              id: '',
              name: '',
              document: '',
            },
            documentValid: '',
            nameValid: '',
            edadValid: '',
            gradoValid: '',
            documentValidError: 'Documento no válido, solo se permiten números',
            formValid : false,
            type: null,
            error: false,
            message: ''
        }
    },
    watch: {
        data_modal: {
            handler(val){
                let document = val.formData.document;
                if(document.length === 0){
                    this.data_modal.documentValid = '';
                }else{
                    this.data_modal.documentValid = this.validateNumbers(document);
                }
                let name = val.formData.name;
                this.data_modal.nameValid = name.length !== 0;
                let edad = val.formData.edad;
                this.data_modal.edadValid = edad.length !== 0;
                let grado = val.formData.grado;
                this.data_modal.gradoValid = grado.length !== 0;


                this.data_modal.formValid = !!(this.data_modal.documentValid === true && this.data_modal.nameValid === true && this.data_modal.gradoValid === true && this.data_modal.edadValid === true);

            },
            deep: true
        }
    },
    methods: {
        nuevo: function (){
            this.data_modal.type = 'new';
            this.showModal();
        },
        showModal: function () {
            $(modal).modal('show');
        },
        hideModal: function (){
            $(modal).modal('hide');
        },
        edit: function (id, name, document, edad, grado){
            this.data_modal.type = 'edit';
            this.data_modal.estudianteToEdit.id = id;
            this.data_modal.estudianteToEdit.name = name;
            this.data_modal.estudianteToEdit.document = document;
            this.data_modal.formData.name = name;
            this.data_modal.formData.document = document;
            this.data_modal.formData.edad = edad;
            this.data_modal.formData.grado = grado;
            this.showModal();
        },
        validateNumbers: function (val) {
            let reg = new RegExp('^[0-9]+$');
            return reg.test(val);
        },
        saveData: function (){
            let data;
            if (this.data_modal.type === 'new'){
                data = {
                    name : this.data_modal.formData.name,
                    document: this.data_modal.formData.document,
                    edad: this.data_modal.formData.edad,
                    grado_id: this.data_modal.formData.grado
                }
            }else{
                data = {
                    id: this.data_modal.estudianteToEdit.id,
                    name : this.data_modal.formData.name,
                    edad: this.data_modal.formData.edad,
                    grado_id: this.data_modal.formData.grado
                }
            }
            this.sendData(data, this.data_modal.type);
        },
        sendData: function (data, type) {
            console.log(data);
            let _URL = (type === 'new') ? '/estudiantes/create/' : '/estudiantes/update/'+data.id;
            axios.post(_URL, data)
                .then( (response) => {
                    console.log(response.data);
                    if (response.data.statusText === 'OK'){
                        location.reload();
                    }else if(response.data.statusText === 'ERROR'){
                        this.data_modal.error = true;
                        this.data_modal.message = response.data.message;
                        // this.clearForm();
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        remove: function (id) {
            Swal.fire({
                title: 'Está seguro de eliminar?',
                text: "Si aceptas se reliminará el registro y no se podrá recuperar",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, Eliminarlo!'
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.post('/estudiantes/delete/'+id)
                        .then((response) => {
                            if (response.data.statusText === 'OK'){
                                Swal.fire(
                                    response.data.message,
                                    '',
                                    'success'
                                ).then(() =>{
                                    location.reload();
                                })
                            }else if(response.data.statusText === 'ERROR'){
                                Swal.fire(
                                    response.data.message,
                                    '',
                                    'error'
                                )
                                this.data_modal.error = true;
                                this.data_modal.message = response.data.message;
                                // this.clearForm();
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                }
            })
        }
    }
});
$(document).ready( function () {
    $('#estudiantes_table').on('init.dt',function() {
        $("#estudiantes_table").removeClass('table-loader').show();
    });
    setTimeout(function(){
        $('#estudiantes_table').DataTable();
    }, 3000);
} );
