const modal = "#modal-result";
new Vue({
    el: "#app-reporte",
    data:{
        message: 'este es el mensaje',
        reportToView : null,
        reporteName: '',
        preguntas : [
            { code : 'temperatura_mayor', pregunta: 'Su temperatura el dia de hoy ha sido igual o mayor a 37.5c'},
            { code : 'tos_seca', pregunta: 'Tos seca'},
            { code : 'dolor_de_garganta', pregunta: 'Dolor de garganta'},
            { code : 'secresion_nasal', pregunta: 'Secreción nasal'},
            { code : 'congestion_nasal', pregunta: 'Congestión nasal'},
            { code : 'malestar_general', pregunta: 'Malestar general'},
            { code : 'dificultad_al_respirar', pregunta: 'Dificultad para respirar'},
            { code : 'perdida_del_olfato', pregunta: 'Perdida de olfato'},
            { code : 'perdida_del_gusto', pregunta: 'Perdida del gusto'},
            { code : 'dolor_muscular', pregunta: 'Dolor muscular'},
            { code : 'dolor_de_cabeza', pregunta: 'Dolor de cabeza'},
            { code : 'nauseas', pregunta: 'Nauseas'},
            { code : 'vomito', pregunta: 'Vómito'},
            { code : 'diarrea', pregunta: 'Diarrea'},
            { code : 'dolor_abdominal_inusual', pregunta: 'Dolor abdominal inusual'},
            { code : 'ojos_rojos', pregunta: 'Ojos rojos'},
            { code : 'estornudos', pregunta: 'Estornudos'},
            { code : 'brote_salpullido', pregunta: 'Brote, salpullido'},
            { code : 'contacto_con_persona_con_covid', pregunta: 'Ha tenido contacto en los últimos 10 dias o convive con alguna persona con COVID-19 positivo'},
            { code : 'diagnosticado_con_covid_o_sospecha', pregunta: 'Ha sido diagnosticado con COVID – 19 o tiene sospecha de tenerlo'}
        ]
    },
    methods: {
        showReport: function (name, edad, grado, resultado) {
            this.reporteName = name;
            this.reportToView = this.formatDataResponse(JSON.parse(resultado));
            $(modal).modal('show');
        },
        formatDataResponse: function (data) {
            let dataFormated = [];
            $.each(this.preguntas, (i, el) => {
                $.each(data, (index, element) => {
                    if(el.code === index){
                        let obj = {code : index, pregunta: el.pregunta, response: element};
                        dataFormated.push(obj);
                    }
                })
            })
            return dataFormated;
        },
        remove: function (reportId) {
            Swal.fire({
                title: 'Está seguro de eliminar?',
                text: "Si aceptas se reliminará el registro y no se podrá recuperar",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, Eliminarlo!'
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.post('/reporte/delete/'+reportId)
                        .then((response) => {
                            if (response.data.statusText === 'OK'){
                                Swal.fire(
                                    response.data.message,
                                    '',
                                    'success'
                                ).then(() =>{
                                    location.reload();
                                })
                            }else if(response.data.statusText === 'ERROR'){
                                Swal.fire(
                                    response.data.message,
                                    '',
                                    'error'
                                )
                                this.data_modal.error = true;
                                this.data_modal.message = response.data.message;
                                // this.clearForm();
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                }
            })
        },
        serchBy: function (e) {
            let valueGrado = e.target.value;
            $("#reporte_table_filter input[type=search]").val(valueGrado);
            $('#reporte_table').DataTable().search(valueGrado).draw();
        },
        clearFilter: function (e) {
            e.preventDefault();
            $("#reporte_table_filter input[type=search]").val('');
            $('#reporte_table').DataTable().search('').draw();
        }
    }
});


$(document).ready( function () {
    $('#reporte_table').on('init.dt',function() {
        $("#reporte_table").removeClass('table-loader').show();
    });
    setTimeout(function(){
        $('#reporte_table').DataTable();
    }, 3000);
} );

