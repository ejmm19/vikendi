<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::user()){
        return redirect('/home');
    }else{
        return view('auth.login');
    }
});

Auth::routes(['register' => false]);


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('onlyadmin');

/*ROUTES DOCENTES*/
Route::get('/docentes',
    '\App\Http\Controllers\Vikendi\DocentesController@index'
)->name('docentes')->middleware('onlyadmin');;

Route::post('/docentes/create', '\App\Http\Controllers\Vikendi\DocentesController@create')->middleware('onlyadmin');;
Route::post('/docentes/update/{id}', '\App\Http\Controllers\Vikendi\DocentesController@update')->middleware('onlyadmin');;
Route::post('/docentes/delete/{id}', '\App\Http\Controllers\Vikendi\DocentesController@destroy')->middleware('onlyadmin');;

/*ROUTES DOCENTES*/

/*ROUTES ESTUDIANTES*/

Route::get('/estudiantes',
    '\App\Http\Controllers\Vikendi\EstudiantesController@index'
)->name('estudiantes')->middleware(['auth']);;

Route::post('/estudiantes/create', '\App\Http\Controllers\Vikendi\EstudiantesController@create')->middleware(['auth']);
Route::post('/estudiantes/update/{id}', '\App\Http\Controllers\Vikendi\EstudiantesController@update')->middleware(['auth']);
Route::post('/estudiantes/delete/{id}', '\App\Http\Controllers\Vikendi\EstudiantesController@destroy')->middleware(['auth']);

/*ROUTES ESTUDIANTES*/

/*ROUTES GRADOS*/

Route::get('/grados',
    '\App\Http\Controllers\Vikendi\GradosController@index'
)->name('grados')->middleware('onlyadmin');

Route::post('/grados/create', '\App\Http\Controllers\Vikendi\GradosController@create')->middleware(['onlyadmin', 'auth']);
Route::post('/grados/delete/{id}', '\App\Http\Controllers\Vikendi\GradosController@destroy')->middleware(['onlyadmin', 'auth']);
Route::post('/grados/update/{id}', '\App\Http\Controllers\Vikendi\GradosController@update')->middleware(['onlyadmin', 'auth']);

/*ROUTES GRADOS*/

Route::get('/reporte',
    '\App\Http\Controllers\Vikendi\ReportesController@index'
)->name('reporte')->middleware(['auth']);

Route::post('/reporte/delete/{id}', '\App\Http\Controllers\Vikendi\ReportesController@destroy')->middleware(['onlyadmin', 'auth']);

Route::get('/visitantes',
    '\App\Http\Controllers\Vikendi\VisitantesController@index'
)->name('visitantes')->middleware(['auth']);

Route::post('/visitantes/create', '\App\Http\Controllers\Vikendi\VisitantesController@create')->middleware(['auth']);
Route::post('/visitantes/update/{id}', '\App\Http\Controllers\Vikendi\VisitantesController@update')->middleware(['auth']);
Route::post('/visitantes/delete/{id}', '\App\Http\Controllers\Vikendi\VisitantesController@destroy')->middleware(['auth']);
