@extends('layouts.app')
@section('title', 'Reporte')
@section('content')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <div id="app-reporte">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="d-flex card-header">
                            <div class="p-2">Reporte</div>
                            <div class="ml-auto p-2">
                                <a href="{{ route('home') }}" class="btn btn-outline-danger">
                                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 pt-5 px-5">
                                <form class="form">
                                    <div class="row">
                                        <div class="col-md-2 col-12 mb-0 mb-sm-2">
                                            <span>Filtrar por: </span>
                                        </div>
                                        <div class="col-md-3 col-12 mb-0 mb-sm-2">
                                            <select name="grado" id="" class="form-control" @change="serchBy">
                                                <option value="" selected disabled>Seleccione un grupo</option>
                                                <option value="Estudiantes">Estudiantes</option>
                                                <option value="Docentes">Docentes</option>
                                                <option value="Visitantes">Visitantes</option>
                                            </select>
                                        </div>

                                        <div class="col-md-3 col-12 mb-0 mb-sm-2">
                                            <select name="grado" id="" class="form-control" @change="serchBy">
                                                <option value="" selected disabled>Seleccione un grado</option>
                                                @foreach($grados as $grado)
                                                    <option value="{{ $grado->name }}">{{ $grado->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-2 col-12 mb-0 mb-sm-2">
                                            <input type="date" @change="serchBy" class="form-control" name="fecha" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
                                        </div>
                                        <div class="col-md-2 col-12 mb-0 mb-sm-2">
                                            <button  class="btn btn-primary btn-block" @click="clearFilter">Limpiar Filtro</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12 p-5">
                                <table id="reporte_table" class="display table-loader dataTable">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nombres y apellidos</th>
                                        <th>Documento</th>
                                        <th>Grupo</th>
                                        <th>Edad</th>
                                        <th>Grado</th>
                                        <th>Resultado</th>
                                        <th>Fecha</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($reportes as $reporte)
                                            @php
                                                $userType = '';
                                                $reponse = json_decode($reporte->response, true);
                                                  $negativas = 0;
                                                  $positivas = 0;
                                                  foreach ($reponse as $item) {
                                                      if ($item === 'no'){
                                                          $negativas++;
                                                      }else{
                                                          $positivas++;
                                                      }
                                                  }
                                                  $classDiag = '';
                                                  if ($classDiag === 1){
                                                      $classDiag = 'leave-status';
                                                  }elseif ($positivas === 2){
                                                      $classDiag = 'warning-status';
                                                  }elseif ($positivas >= 3){
                                                      $classDiag = 'danger-status';
                                                  }
                                            @endphp
                                            <tr class="{{ $classDiag }}">
                                                <td>{{ $reporte->id }}</td>
                                                <td>{{ $reporte->name }}</td>
                                                <td>
                                                  @php
                                                    $document = '---';
                                                    if($reporte->idestu){
                                                        $document = $reporte->idestu;
                                                        $userType = 'Estudiantes';
                                                    }elseif($reporte->idvis){
                                                        $document = $reporte->idvis;
                                                        $userType = 'Visitantes';
                                                    }else{
                                                        $document = $reporte->iddocente;
                                                        $userType = 'Docentes';
                                                    }
                                                    echo $document;
                                                  @endphp
                                                </td>
                                                <td>
                                                    {{ $userType }}
                                                </td>
                                                <td>
                                                @php
                                                    $edad = "---";
                                                    if($reporte->edadestu){
                                                        $edad =  $reporte->edadestu;
                                                    }elseif($reporte->edadvis){
                                                        $edad =  $reporte->edadvis;
                                                    }else{
                                                        $edad =  $reporte->edaddoc;
                                                    }
                                                    echo $edad;
                                                  @endphp
                                                </td>
                                                <td>
                                                @php
                                                    $grado = '---';
                                                    if($reporte->grado){
                                                        $grado = $reporte->grado;
                                                    }elseif($reporte->gradoDocente){
                                                        $grado = $reporte->gradoDocente;
                                                    }
                                                    echo $grado;
                                                  @endphp
                                                </td>
                                                <td>
                                                    {{ "Positivas: ".$positivas." Negativas: ".$negativas }}
                                                </td>
                                                <td>
                                                    @php
                                                        $date = new DateTime($reporte->created_at);
                                                    @endphp
                                                    {{ $reporte->created_at }}
                                                </td>
                                                <td>
                                                    <a href="#" class="text-info p-2" @click="showReport('{{ $reporte->name }}', '{{ $edad }}', '{{$grado }}', '{{ $reporte->response }}' )">
                                                        <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                                                    </a>
                                                    @if(\Illuminate\Support\Facades\Session::get('isAdmin'))
                                                        <a href="#" class="text-danger p-2" title="Eliminar"  @click="remove('{{ $reporte->report_id }}')">
                                                            <i class="fa fa-times fa-lg" aria-hidden="true"></i>
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade bd-example-modal-lg" id="modal-result" tabindex="-1" role="dialog" aria-labelledby="modal-result" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            Respuestas de: @{{ reporteName }}
                        </h5>
                    </div>
                        <div class="modal-body">
                            <div class="container-fluid">
                                <div class="row">
                                    <template v-for="reporte in reportToView">
                                        <div class="col-11">
                                            @{{ reporte.pregunta }}
                                        </div>
                                        <div class="col-1">
                                            @{{ reporte.response }}
                                        </div>
                                    </template>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ asset('js/reporte.js') }}"></script>
        <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
@endsection
