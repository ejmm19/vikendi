@extends('layouts.app')
@section('title', 'Estudiantes')
@section('content')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <div id="app-estudiantes">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="d-flex card-header">
                            <div class="p-2">Estudiantes</div>
                            <div class="ml-auto p-2">
                                <a href="{{ route('home') }}" class="btn btn-outline-danger">
                                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                </a>
                                <a href="#" @click="nuevo()"  class="btn btn-outline-info">
                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 p-5">
                                <table id="estudiantes_table" class="display table-loader dataTable">
                                    <thead>
                                    <tr>
                                        <th>Nombres y apellidos</th>
                                        <th>Identificación</th>
                                        <th>Edad</th>
                                        <th>Grado</th>
                                        <th>Fecha de creación</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($estudiantes as $estudiante)
                                        <tr>
                                            <td>{{ $estudiante->name }}</td>
                                            <td>{{ $estudiante->identification }}</td>
                                            <td>{{ $estudiante->edad }}</td>
                                            <td>{{ $estudiante->grado_name }}</td>
                                            <td>{{ $estudiante->created_at }}</td>
                                            <td>
                                                <a href="#" class="text-info p-2" title="Editar" @click="edit('{{ $estudiante->id }}', '{{ $estudiante->name }}', '{{ $estudiante->identification }}', '{{ $estudiante->edad }}', '{{ $estudiante->grado_id }}')" ><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></a>
                                                <a href="#" class="text-danger p-2" title="Eliminar"  @click="remove('{{ $estudiante->id }}')" ><i class="fa fa-times fa-lg" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="add-edit-estudiantes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            <span v-if="data_modal.type === 'new'">Agregar Estudiante</span>
                            <span v-else>Editar Estudiante: @{{ data_modal.estudianteToEdit.name }}</span>
                        </h5>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="name">Nombres y apellidos</label>
                            <input type="text" class="form-control" id="name" v-model="data_modal.formData.name"
                                   placeholder="Ingrese el nombre del estudiante">
                        </div>
                        <div class="form-group">
                            <label for="docentedoc">Documento de identidad</label>
                            <input type="text" class="form-control" id="docentedoc" v-model="data_modal.formData.document"
                                   placeholder="Ingrese el documento" :readonly="data_modal.type === 'edit'">
                            <small class="text-danger" v-if="data_modal.documentValid !== '' && !data_modal.documentValid">
                                @{{ data_modal.documentValidError }}
                            </small>
                        </div>

                        <div class="form-group">
                            <label for="docentedoc">Edad</label>
                            <select v-model="data_modal.formData.edad" class="form-control">
                                <option value="" selected disabled>Seleccioned la edad</option>
                                @for($i=1;$i<=60;$i++)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                @endfor
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="#">Grado</label><br>
                            <select v-model="data_modal.formData.grado" class="form-control">
                                <option selected disabled value="">Seleccione un grado</option>
                                @foreach($grados as $grado)
                                    <option value="{{ $grado->id }}">{{ $grado->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <small class="text-danger" v-if="data_modal.error">@{{ data_modal.message }}</small>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" @click="hideModal">Cancelar</button>
                        <button type="button" class="btn btn-primary" :disabled="!data_modal.formValid" @click="saveData">
                            Guardar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/estudiantes.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
@endsection
