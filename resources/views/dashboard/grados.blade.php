@extends('layouts.app')
@section('title', 'Grados')
@section('content')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <div id="app-grados">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="d-flex card-header">
                            <div class="p-2">Grados</div>
                            <div class="ml-auto p-2">
                                <a href="{{ route('home') }}" class="btn btn-outline-danger">
                                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                </a>
                                <a href="#" @click="nuevo($event)"  class="btn btn-outline-info">
                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 p-5">
                                <table id="grados_table" class="display table-loader dataTable">
                                    <thead>
                                    <tr>
                                        <th>Código</th>
                                        <th>Grado</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($grados as $grado)
                                        <tr>
                                            <td>{{ $grado->id }}</td>
                                            <td>{{ $grado->name }}</td>
                                            <td>
                                                <a href="#" class="text-info p-2" title="Editar" @click="edit('{{ $grado->id }}', '{{ $grado->name }}')"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></a>
                                                <a href="#" class="text-danger p-2" title="Eliminar"  @click="remove('{{ $grado->id }}')"><i class="fa fa-times fa-lg" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>



        <!-- Modal -->
        <div class="modal fade" id="add-edit-grado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            <span v-if="data_modal.type === 'new'">Agregar grado</span>
                            <span v-else>Editar grado: @{{ data_modal.gradoToEdit.name }}</span>
                        </h5>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Grado</label>
                            <input type="hidden" class="form-control" id="namegrado" v-model="data_modal.formData.name"
                                   placeholder="Ingrese el nombre del grado">
                        </div>

                        <div class="form-group">
                            <select v-model="data_modal.selectGrado" @change="validate">
                                <option selected disabled value="">Seleccione un grado</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                                <option>6</option>
                                <option>7</option>
                                <option>8</option>
                                <option>9</option>
                                <option>10</option>
                                <option>11</option>
                                <option>Formación Complementaria</option>
                            </select>
                            <select v-model="data_modal.selectGrupo" @change="validate" :disabled="data_modal.selectGrado === ''">
                                <option disabled value="">Seleccione un grupo</option>
                                <optgroup label="Grupo Letras">
                                    <option>A</option>
                                    <option>B</option>
                                    <option>C</option>
                                    <option>D</option>
                                    <option>E</option>
                                    <option>F</option>
                                </optgroup>
                                <optgroup label="Grupo Números">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    <option>6</option>
                                </optgroup>
                                <optgroup label="Semestres">
                                    <option>Semestre I</option>
                                    <option>Semestre II</option>
                                    <option>Semestre III</option>
                                    <option>Semestre IV</option>
                                    <option>Semestre V</option>
                                </optgroup>
                            </select>
                        </div>
                        <div class="form-group">
                            <small class="text-danger" v-if="data_modal.error">@{{ data_modal.message }}</small>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" @click="hideModal">Cancelar</button>
                        <button type="button" class="btn btn-primary" :disabled="data_modal.formData.name === ''" @click="saveData" >
                            Guardar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/grados.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

@endsection
