@extends('layouts.app')
@section('title', 'Home')
@section('content')
<div class="container">
    <div class="row justify-content-center animated fadeIn">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Panel Inicial</div>
                <div class="row my-5">
                    @if(\Illuminate\Support\Facades\Session::get('isAdmin'))
                        <div class="col-md-3 text-center py-3 content-card-dash">
                            <a href="{{ route('docentes') }}" class="text-body">
                                <div class="card py-5 card-dash">
                                    <i class="fa fa-folder-open fa-2x" aria-hidden="true"></i>
                                    <h4>Docentes</h4>
                                </div>
                            </a>
                        </div>
                    @endif

                    <div class="col-md-3 text-center py-3 content-card-dash">
                        <a href="{{ route('estudiantes') }}" class="text-body">
                            <div class="card py-5 card-dash">
                                <i class="fa fa-folder-open fa-2x" aria-hidden="true"></i>
                                <h4>Estudiantes</h4>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3 text-center py-3 content-card-dash">
                        <a href="{{ route('visitantes') }}" class="text-body">
                            <div class="card py-5 card-dash">
                                <i class="fa fa-folder-open fa-2x" aria-hidden="true"></i>
                                <h4>Visitantes</h4>
                            </div>
                        </a>
                    </div>
                    @if(\Illuminate\Support\Facades\Session::get('isAdmin'))
                        <div class="col-md-3 text-center py-3 content-card-dash">
                            <a href="{{ route('grados') }}" class="text-body">
                                <div class="card py-5 card-dash">
                                    <i class="fa fa-folder-open fa-2x" aria-hidden="true"></i>
                                    <h4>Grados</h4>
                                </div>
                            </a>
                        </div>
                    @endif

                    <div class="col-md-3 text-center py-3 content-card-dash">
                        <a href="{{ route('reporte') }}" class="text-body">
                            <div class="card py-5 card-dash">
                                <i class="fa fa-folder-open fa-2x" aria-hidden="true"></i>
                                <h4>Reporte</h4>
                            </div>
                        </a>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
