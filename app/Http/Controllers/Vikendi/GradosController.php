<?php

namespace App\Http\Controllers\Vikendi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class GradosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $grados = DB::table('grados')->get();
        return view('dashboard.grados')->with(['grados'=>$grados]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array
     */
    public function create(Request $request)
    {
        if (!empty($request->name)){

            $haveData =  DB::table('grados')->where('name', $request->name)->exists();
            if(!$haveData){
                DB::table('grados')->insert(['name'=> $request->name]);
                $message = __('Datos agregados correctamente');
                $statusText = "OK";
            }else{
                $message = __('El grado ya existe, por favor intente con otro');
                $statusText = "ERROR";
            }
        }else{
            $message = __('Se prodjo un error al guardar los datos');
            $statusText = "ERROR";
        }
        return array(
            "message" => $message,
            "statusText" => $statusText
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return array
     */
    public function update(Request $request, $id)
    {
        $haveDataIndice =  DB::table('grados')->where('id', $id)->exists();

        if (!empty($haveDataIndice)){
            try{
                DB::table('grados')->where('id', $id)->update(['name'=>$request->name]);
                $message = __('Datos eliminados correctamente');
                $statusText = "OK";
            }catch (\Exception $e){
                $message = __('Problemas al Actualizar el registro.');
                $statusText = "ERROR";
            }
        }else{
            $message = __('Problemas al Actualizar el registro.');
            $statusText = "ERROR";
        }

        return array(
            "message" => $message,
            "statusText" => $statusText
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array
     */
    public function destroy($id)
    {
        $haveData =  DB::table('grados')->where('id', $id)->exists();

        if (!empty($haveData)){
            DB::table('grados')->where('id', $id)->delete();
            $message = __('Datos eliminados correctamente');
            $statusText = "OK";
        }else{
            $message = __('Problemas al eliminar el registro.');
            $statusText = "ERROR";
        }

        return array(
            "message" => $message,
            "statusText" => $statusText
        );
    }
}
