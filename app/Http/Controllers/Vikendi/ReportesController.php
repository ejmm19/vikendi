<?php

namespace App\Http\Controllers\Vikendi;

use DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ReportesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $grados = DB::table('grados')->get(['id', 'name']);
        $reportes = DB::table('users')
                    ->leftJoin('user_estudiante', 'users.id', '=', 'user_estudiante.user_id')
                    ->leftJoin('user_visitante', 'users.id', '=', 'user_visitante.user_id')
                    ->leftJoin('user_docente', 'users.id', '=', 'user_docente.user_id')
                    ->rightJoin('reporte', 'users.id',  '=', 'reporte.user_id')
                    ->leftJoin('grados', 'user_estudiante.grado_id',  '=', 'grados.id')
                    ->leftJoin('grados as grDoc', 'user_docente.grado_id',  '=', 'grDoc.id')
                    ->select('reporte.id as report_id','users.id', 'users.name', 'user_estudiante.edad as edadestu', 'user_visitante.edad as edadvis', 'user_docente.edad as edaddoc',
                        'user_estudiante.identification as idestu', 'user_visitante.identification as idvis', 'user_docente.identification as iddocente', 'grados.name as grado', 'grDoc.name as gradoDocente',
                                    'reporte.response', 'reporte.created_at')
                    ->get();


        return view('dashboard.reporte')->with(['reportes'=> $reportes, 'grados' => $grados]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array
     */
    public function destroy($id)
    {
        $haveData =  DB::table('reporte')->where('id', $id)->exists();

        if (!empty($haveData)){
            DB::table('reporte')->where('id', $id)->delete();
            $message = __('Datos eliminados correctamente');
            $statusText = "OK";
        }else{
            $message = __('Problemas al eliminar el registro.');
            $statusText = "ERROR";
        }

        return array(
            "message" => $message,
            "statusText" => $statusText
        );
    }
}
