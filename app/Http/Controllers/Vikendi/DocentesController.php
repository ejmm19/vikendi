<?php

namespace App\Http\Controllers\Vikendi;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DocentesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $grados = DB::table('grados')->get(['id', 'name']);
        $docentes = DB::table('user_docente')
            ->join('users', 'user_docente.user_id', '=', 'users.id')
            ->join('grados', 'user_docente.grado_id', '=', 'grados.id')
            ->select('users.id', 'users.name', 'users.email', 'user_docente.identification', 'user_docente.edad', 'grados.name as grado_name', 'user_docente.grado_id', 'users.created_at')
            ->get();
        return view('dashboard.docentes')->with(['grados' => $grados, 'docentes' => $docentes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array
     */
    public function create(Request $request)
    {
        if (!empty($request->all())){

            $haveUser =  DB::table('users')->where('email', $request->email)->exists();

            if(!$haveUser){
                User::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => Hash::make($request->document),
                ]);
                $getId =  DB::table('users')->select('id')->where('email', $request->email)->first();

                DB::table('user_docente')->insert([
                    'user_id'=> $getId->id,
                    'grado_id'=> $request->grado_id,
                    'identification'=> $request->document,
                    'edad'=> $request->edad,
                ]);
                $message = __('Datos agregados correctamente');
                $statusText = "OK";
            }else{
                $message = __('El Docente ya se encuentra registrado');
                $statusText = "ERROR";
            }
        }else{
            $message = __('Se prodjo un error al guardar los datos');
            $statusText = "ERROR";
        }
        return array(
            "message" => $message,
            "statusText" => $statusText
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return array
     */
    public function update(Request $request, $id)
    {
        $haveUser =  DB::table('users')->where('id', $id)->exists();

        if (!empty($haveUser)){
            try{
                DB::table('users')->where('id', $id)->update(['name'=>$request->name]);
                DB::table('user_docente')->where('user_id', $id)->update(['grado_id'=>$request->grado_id, 'edad' => $request->edad]);
                $message = __('Datos eliminados correctamente');
                $statusText = "OK";
            }catch (\Exception $e){
                $message = __('Problemas al actualizar el registro.');
                $statusText = "ERROR";
            }
        }else{
            $message = __('Problemas al eliminar el registro.');
            $statusText = "ERROR";
        }

        return array(
            "message" => $message,
            "statusText" => $statusText
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array
     */
    public function destroy($id)
    {
        $haveData =  DB::table('users')->where('id', $id)->exists();

        if (!empty($haveData)){
            DB::table('users')->where('id', $id)->delete();
            $message = __('Datos eliminados correctamente');
            $statusText = "OK";
        }else{
            $message = __('Problemas al eliminar el registro.');
            $statusText = "ERROR";
        }

        return array(
            "message" => $message,
            "statusText" => $statusText
        );
    }
}
