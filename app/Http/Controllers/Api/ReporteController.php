<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ReporteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string[]
     */
    public function store(Request $request)
    {
        $message = "Ocurrió un error al guardar los datos";
        $status = "ERROR";
        $arrayValores = array_values($request->responses);
        $msjQuedateEnCasa = (in_array('si', $arrayValores)) ? 'Por la seguridad tuya y de los demás, te recomendamos que te quedes en casa' : 'El custionario se guardó con éxito.';
        if ($request->all()){
            try{
                DB::table('reporte')->insert([
                    'user_id' => $request->user_id,
                    'response' => json_encode($request->responses)
                ]);
                $message = "$msjQuedateEnCasa";
                $status = "OK";
            }catch (\Exception $e){
                $message = "Ocurrió un error al guardar los datos";
                $status = "ERROR";
            }
        }
        $dataToreturn  = [
            'status' => $status,
            'message' => $message
        ];
        Log::info(json_encode($dataToreturn));
        return $dataToreturn;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return bool|bool[]|false[]|\Illuminate\Http\Response
     */
    public function show($id)
    {
        //validate si ha pasado 24 horas
        $canResponse = true;
        $dteDiff = '';
        $ultimoReporte = DB::table('reporte')
            ->where('user_id', $id)
            ->orderBy('created_at', 'DESC')
            ->first();
        if (!empty($ultimoReporte)){
            $date1 = new \DateTime("now");
            $date2 = new \DateTime($ultimoReporte->created_at);

            $dteDiff  = $date1->diff($date2)->format("%H");
            $dteDiff  = intval($dteDiff);
            if($dteDiff <= 24){
                $canResponse = false;
            }
        }
        return [
            'canresponse' => $canResponse,
            'dteDiff' => $dteDiff,
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
