<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class EstudiantesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return string[]
     */
    public function index()
    {
        return [
            'usuer' => 'user007',
            'edad' => '19'
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function store(Request $request)
    {
        try{
            $haveUser =  DB::table('user_estudiante')
                ->select('user_id')
                ->where('identification','=', $request->document)->first();

            $haveUserVisitante =  DB::table('user_visitante')
                ->select('user_id')
                ->where('identification','=', $request->document)->first();


            $haveUserDocente =  DB::table('user_docente')
                ->select('user_id')
                ->where('identification','=', $request->document)->first();


            if ($haveUser){
                $passrwordCurrentUser = DB::table('users')->where('id', $haveUser->user_id)->first();
                $validatePassword = Hash::check($request->password, $passrwordCurrentUser->password);
                if ($validatePassword){
                    $usuarioLoggIn = DB::table('users')
                        ->join('user_estudiante', 'users.id', '=', 'user_estudiante.user_id')
                        ->join('grados', 'user_estudiante.grado_id', '=', 'grados.id')
                        ->select('users.id', 'users.name', 'users.password', 'user_estudiante.identification', 'user_estudiante.edad', 'grados.name as grado_name', 'users.created_at')
                        ->where('users.id', '=', $haveUser->user_id)
                        ->first();
                    $message = "Datos consultados con éxito.";
                    $status = "OK";
                }else{
                    $usuarioLoggIn = null;
                    $message = "Contraseña incorrécta.";
                    $status = "ERROR";
                }
            }elseif($haveUserVisitante){
                $passrwordCurrentUser = DB::table('users')->where('id', $haveUserVisitante->user_id)->first();
                $validatePassword = Hash::check($request->password, $passrwordCurrentUser->password);
                if ($validatePassword){
                    $usuarioLoggIn = DB::table('users')
                        ->join('user_visitante', 'users.id', '=', 'user_visitante.user_id')
                        ->select('users.id', 'users.name', 'users.password', 'user_visitante.identification', 'user_visitante.edad', 'users.created_at')
                        ->where('users.id', '=', $haveUserVisitante->user_id)
                        ->first();
                    $message = "Datos consultados con éxito.";
                    $status = "OK";
                }else{
                    $usuarioLoggIn = null;
                    $message = "Contraseña incorrécta.";
                    $status = "ERROR";
                }
            }elseif($haveUserDocente){
                $passrwordCurrentUser = DB::table('users')->where('id', $haveUserDocente->user_id)->first();
                $validatePassword = Hash::check($request->password, $passrwordCurrentUser->password);
                if ($validatePassword){
                    $usuarioLoggIn = DB::table('users')
                        ->join('user_docente', 'users.id', '=', 'user_docente.user_id')
                        ->select('users.id', 'users.name', 'users.password', 'user_docente.identification',  'users.created_at')
                        ->where('users.id', '=', $haveUserDocente->user_id)
                        ->first();
                    $message = "Datos consultados con éxito.";
                    $status = "OK";
                }else{
                    $usuarioLoggIn = null;
                    $message = "Contraseña incorrécta.";
                    $status = "ERROR";
                }
            }else{
                $usuarioLoggIn = null;
                $message = "Usuario no registrado, pongase en contacto con su docente o persona encargada.";
                $status = "ERROR";
            }
        }catch (\Exception $e){
            $usuarioLoggIn = $e->getMessage();
            $message = "Ocurrió un error en la consulta.";
            $status = "ERROR";
        }
        return [
            'data' => $usuarioLoggIn,
            'status' => $status,
            'message' => $message
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
