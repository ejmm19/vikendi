<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class OnlyAdminValid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userId = Auth::id();
        $isDocente = DB::table('user_docente')->where('user_id', $userId)->exists();
        $isEstudiante = DB::table('user_estudiante')->where('user_id', $userId)->exists();
        if($isDocente || $isEstudiante){
            $request->session()->put('isAdmin', false);
            if (!route('home')){
                return redirect('home');
            }
        }else{
            $request->session()->put('isAdmin', true);
        }
        return $next($request);
    }
}
